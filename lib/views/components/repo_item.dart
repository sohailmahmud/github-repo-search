import 'package:flutter/material.dart';
import 'package:github_repo_search/models/repo.dart';
import 'package:github_repo_search/providers/theme_provider.dart';

class RepoItem extends StatelessWidget {
  const RepoItem({
    super.key,
    required this.themeProvider,
    required this.repo,
    this.onTap,
    this.isSelected = false,
  });

  final ThemeProvider themeProvider;
  final bool isSelected;
  final Repo repo;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
          onTap: onTap,
          highlightColor: Colors.lightBlueAccent,
          splashColor: Colors.red,
          child: Container(
            padding:
            const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    repo.name ?? '',
                    style: themeProvider.themeData().textTheme.bodyLarge,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(repo.description ?? '',
                        style: themeProvider.themeData().textTheme.bodyMedium),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text(repo.owner!.login!,
                                textAlign: TextAlign.start,
                                style: themeProvider.themeData().textTheme.bodySmall)),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              const Icon(Icons.star, color: Colors.deepOrange,),
                              Padding(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: Text(repo.updatedAt.toString(),
                                    textAlign: TextAlign.center,
                                    style: themeProvider.themeData().textTheme.bodySmall),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            child: Text(repo.language ?? '',
                                textAlign: TextAlign.end,
                                style: themeProvider.themeData().textTheme.bodySmall)),
                      ],
                    ),
                  ),
                ]),
          )),
    );
  }
}