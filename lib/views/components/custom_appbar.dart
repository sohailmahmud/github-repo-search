import 'package:flutter/material.dart';
import 'package:github_repo_search/providers/theme_provider.dart';
import 'package:github_repo_search/utilities/utility.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class CustomAppBar extends StatelessWidget {
  CustomAppBar({Key? key}) : super(key: key);

  ThemeProvider themeProvider = Provider.of<ThemeProvider>(Utility.context);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: themeProvider.themeColor().cardColor,
      elevation: 0,
      title: Text(
        "Repository Search",
        style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w500,
            color: themeProvider.themeColor().textColor),
      ),
      actions: [
        IconButton(
          onPressed: () {
            themeProvider.toggleThemeData();
          },
          icon: Icon(themeProvider.isLightTheme
              ? Icons.dark_mode
              : Icons.light_mode),
        ),
      ],
    );
  }
}
