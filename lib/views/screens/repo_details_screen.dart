import 'package:flutter/material.dart';
import 'package:github_repo_search/models/repo.dart';
import 'package:github_repo_search/utilities/utility.dart';

class RepoDetailsScreen extends StatefulWidget {
  const RepoDetailsScreen({Key? key, required this.repo}) : super(key: key);

  final Repo repo;

  @override
  _RepoDetailsState createState() => _RepoDetailsState();
}

class _RepoDetailsState extends State<RepoDetailsScreen> {
  bool _isStarred = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.repo.name!} repository details'),
        actions: [
          IconButton(
            icon: const Icon(Icons.star),
            onPressed: () {
              setState(() {
                _isStarred = !_isStarred;
              });
            },
            color: _isStarred ? Colors.amber : null,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(height: 16),
            CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage(widget.repo.owner!.avatarUrl!),
            ),
            const SizedBox(height: 8),
            Text('Repo owner\'s name: ${widget.repo.owner!.login}'),
            const SizedBox(height: 8),
            Text('Description: ${widget.repo.description}'),
            const SizedBox(height: 8),
            Text('Last updated: ${Utility.formatDateTime(DateTime.parse(widget.repo.updatedAt!))}'),
            const SizedBox(height: 8),
          ],
        ),
      ),
    );
  }
}
