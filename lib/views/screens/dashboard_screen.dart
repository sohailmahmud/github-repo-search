import 'package:flutter/material.dart';
import 'package:github_repo_search/providers/repo_provider.dart';
import 'package:github_repo_search/providers/theme_provider.dart';
import 'package:github_repo_search/views/components/custom_appbar.dart';
import 'package:provider/provider.dart';

class DashboardScreen extends StatefulWidget {
  static const String routeName = "/DashboardScreen";

  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  late ThemeProvider themeProvider;
  late RepoProvider repoProvider;
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   //Utility.showLoadingDialog();
    // });
  }

  @override
  Widget build(BuildContext context) {
    themeProvider = Provider.of<ThemeProvider>(context);
    repoProvider = Provider.of<RepoProvider>(context);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: CustomAppBar(),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 30,
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                children: [
                  const SizedBox(
                    height: 12,
                  ),
                  const Text(
                    "Search for any Github repository and get the details of the repository.",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, "/SearchScreen");
                    },
                    child: SizedBox(
                      height: 50,
                      child: TextFormField(
                        enabled: false,
                        cursorColor: themeProvider.themeColor().textColor,
                        decoration: InputDecoration(
                            hintText: "Search Github Repositories",
                            hintStyle: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: themeProvider.themeColor().textColor),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: themeProvider.themeColor().textColor.withOpacity(0.5), width: 1)),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: themeProvider.themeColor().textColor.withOpacity(0.5), width: 1)),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(color: themeProvider.themeColor().textColor.withOpacity(0.5), width: 1)),
                            suffixIcon: Icon(
                              Icons.search,
                              color: themeProvider.themeColor().textColor,
                            )),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}