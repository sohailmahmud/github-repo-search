
import 'package:flutter/material.dart';
import 'package:github_repo_search/main.dart';
import 'package:github_repo_search/providers/theme_provider.dart';
import 'package:github_repo_search/utilities/colors.dart';
import 'package:github_repo_search/utilities/utility.dart';
import 'package:github_repo_search/views/screens/dashboard_screen.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class SplashScreen extends StatefulWidget {
  static const String routeName = "/SplashScreen";

  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  ThemeProvider themeProvider = Provider.of<ThemeProvider>(RootApp.navKey.currentContext!);

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   //Utility.showLoadingDialog();
    //   DefaultDialogs.showUpdateDialog();
    // });
    //

    Future.delayed(const Duration(seconds: 2),(){
      getStarted();
    });
  }

  getStarted() async{
    if(await Utility.isInternetConnected()){
      Navigator.pushNamedAndRemoveUntil(context, DashboardScreen.routeName, (route) => false);
    }else{
      Utility.showSnackBar("No Internet Connected",durationInSeconds: 20);
    }
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: SizedBox(
          child: Center(
            child: Shimmer.fromColors(
                baseColor: AppColor.defaultColor,
                highlightColor: AppColor.black4,
                child: const Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // SvgPicture.asset(
                    //   AppSvg.getPath("logo"),
                    //   fit: BoxFit.cover,
                    //   height: 100,
                    // ),
                     Text("GitHub Repo Search",style: TextStyle(fontSize: 35,fontWeight: FontWeight.w700),),
                  ],
                )),
          ),
        )
    );
  }
}