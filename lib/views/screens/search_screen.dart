import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:github_repo_search/models/repo.dart';
import 'package:github_repo_search/providers/theme_provider.dart';
import 'package:github_repo_search/repositories/repo_repository.dart';
import 'package:github_repo_search/utilities/utility.dart';
import 'package:github_repo_search/views/components/repo_item.dart';
import 'package:github_repo_search/views/screens/repo_details_screen.dart';
import 'package:provider/provider.dart';

class SearchScreen extends StatefulWidget {
  static const String routeName = "/SearchScreen";

  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  ThemeProvider themeProvider = ThemeProvider();
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    searchController.text = "flutter";
  }

  @override
  Widget build(BuildContext context) {
    themeProvider = Provider.of<ThemeProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: SizedBox(
          child: TextField(
            decoration: InputDecoration(
              hintText: "Search",
              hintStyle: TextStyle(
                color: themeProvider.themeColor().textColor,
              ),
              border: InputBorder.none,
            ),
            style: TextStyle(
              color: themeProvider.themeColor().textColor,
            ),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.search),
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            const SizedBox(
              height: 12,
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: [
                    const Text(
                      "Search for any Github repository and get the details of the repository.",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Expanded(
                      child: FutureBuilder(
                        future: RepoRepository.getRepos(searchController.text),
                        builder: (context, snapshot) {
                          if(snapshot.data != null && snapshot.data!.data != null){
                            List<Repo> repos = snapshot.data!.data as List<Repo>;
                            return ListView.builder(
                              itemCount: repos.length,
                              itemBuilder: (context, index) {
                                Repo repo = repos[index];
                                return RepoItem(
                                  themeProvider: themeProvider,
                                  repo: repo,
                                  onTap: () {
                                    Utility.context
                                        .read<ThemeProvider>()
                                        .setSelectedRepo(repo);
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) => RepoDetailsScreen(
                                          repo: repo,
                                        ),
                                      ),
                                    );
                                  },
                                );
                              },
                            );
                          }
                          else if(snapshot.data != null){
                            return const Center(
                              child: Text("No data found"),
                            );
                          }
                          else{
                            return const Center(
                              child: SpinKitCircle(
                                size: 50,
                                color: Colors.blue,
                              ),
                            );
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}