import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:github_repo_search/providers/repo_provider.dart';
import 'package:github_repo_search/providers/theme_provider.dart';
import 'package:github_repo_search/utilities/size_config.dart';
import 'package:github_repo_search/views/screens/dashboard_screen.dart';
import 'package:github_repo_search/views/screens/search_screen.dart';
import 'package:github_repo_search/views/screens/splash_screen.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:path_provider/path_provider.dart' as path_provider;


void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // if(flavor == "Development"){
    await dotenv.load(fileName: ".env.dev");
  // }

  bool isLightTheme = true;
  ThemeProvider themeProvider = ThemeProvider();
  RepoProvider repoProvider = RepoProvider();
  themeProvider.setTheme(isLightTheme);

  try {
    final appDocumentDirectory =
    await path_provider.getApplicationDocumentsDirectory();
    Hive.init(appDocumentDirectory.path);

    final settings = await Hive.openBox('settings');
    var brightness = SchedulerBinding.instance.platformDispatcher.platformBrightness;
    bool isLightModeOn = brightness == Brightness.light;
    isLightTheme = settings.get('isLightTheme') ?? isLightModeOn;
    themeProvider.setTheme(isLightTheme);
  } catch (e) {
    print(e);
  }

  runApp(MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => themeProvider),
          ChangeNotifierProvider(create: (_) => repoProvider),
        ],
    child: const AppStart(),
  ));
}

class AppStart extends StatelessWidget {
  const AppStart({super.key});

  @override
  Widget build(BuildContext context) {
    return const RootApp();
  }
}

class RootApp extends StatefulWidget {
  static GlobalKey<NavigatorState> navKey = GlobalKey<NavigatorState>();
  const RootApp({Key? key}) : super(key: key);

  @override
  _RootAppState createState() => _RootAppState();
}

class _RootAppState extends State<RootApp> {
  ThemeProvider? themeProvider;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    themeProvider = Provider.of<ThemeProvider>(context);
    themeProvider!.getCurrentStatusNavigationBarColor();

    return LayoutBuilder(
      builder: (context, constraints){
        return OrientationBuilder(
          builder: (context, orientation){
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              title: 'Github Repo Search',
              navigatorKey: RootApp.navKey,
              debugShowCheckedModeBanner: false,
              theme: themeProvider!.themeData(),
              themeMode: themeProvider!.isLightTheme ? ThemeMode.light : ThemeMode.dark,
              navigatorObservers: const [],
              initialRoute: SplashScreen.routeName,
              onGenerateRoute: (settings) {
                switch (settings.name) {
                  case SplashScreen.routeName:
                    return MaterialPageRoute(builder: (_) => const SplashScreen());
                  case DashboardScreen.routeName:
                    return MaterialPageRoute(builder: (_) => const DashboardScreen());
                  case SearchScreen.routeName:
                    return MaterialPageRoute(builder: (_) => const SearchScreen());
                }
                return null;
              },
            );
          },
        );
      }
    );
  }
}
