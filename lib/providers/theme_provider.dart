import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:github_repo_search/utilities/colors.dart';
import 'package:hive/hive.dart';


class ThemeProvider with ChangeNotifier {
  bool isLightTheme = true;

  //singleton
  static final _instance = ThemeProvider._internal();
  factory ThemeProvider(){
    return _instance;
  }
  ThemeProvider._internal();

  void setTheme(isLightTheme){
    this.isLightTheme = isLightTheme;
    print(isLightTheme);
  }

  // the code below is to manage the status bar color when the theme changes
  void getCurrentStatusNavigationBarColor() {
    if (isLightTheme) {
      SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark,
        // systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
      ));
    } else {
      SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.light,
        // systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
      ));
    }

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  // use to toggle the theme
  toggleThemeData() async {
    try{
      final settings = await Hive.openBox('settings');
      settings.put('isLightTheme', !isLightTheme);
    }catch(e){
      print(e);
    }
    isLightTheme = !isLightTheme;
    getCurrentStatusNavigationBarColor();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      notifyListeners();
    });
  }

  // Global theme data we are always check if the light theme is enabled #isLightTheme
  ThemeData themeData() {
    return ThemeData(
        fontFamily: "HelveticaNowText",
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: isLightTheme ? Colors.white : Colors.black /*Color(0xFF26242e)*/,
        brightness: isLightTheme ? Brightness.light : Brightness.dark,
        appBarTheme: AppBarTheme(
            color: isLightTheme ? Colors.white : Colors.black,
            iconTheme: IconThemeData(color: themeColor().textColor),
            titleTextStyle: TextStyle(
                color: themeColor().textColor,
                fontSize: 18,
                fontWeight: FontWeight.w700
            )
        ),
        scaffoldBackgroundColor: themeColor().backgroundColor,
        cardColor: themeColor().cardColor,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        pageTransitionsTheme: const PageTransitionsTheme(builders: {TargetPlatform.android:FadeUpwardsPageTransitionsBuilder(),TargetPlatform.iOS:FadeUpwardsPageTransitionsBuilder()}),
        colorScheme: ColorScheme(
            primary: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            secondary: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            surface: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            background: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            error: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            onPrimary: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            onSecondary: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            onSurface: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            onBackground: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            onError: isLightTheme ? AppColor.fullWhite : AppColor.fullBlack,
            brightness: isLightTheme ? Brightness.light : Brightness.dark
        ),
    );
  }

  // Theme mode to display unique properties not cover in theme data
  ThemeColor themeColor() {
    return ThemeColor(
      defaultColor: AppColor.defaultColor,
      textColor: isLightTheme ? const Color(0xFF000000) /*Color(0xFF0D1418)*/ : const Color(0xFFFFFFFF),
      cardColor: isLightTheme ?  const Color(0xFFFFFFFF): const Color(0xFF000000) /*Color(0xFF19181f)*/,
      backgroundColor: isLightTheme ? const Color(0xFFF2F2FA) : const Color(0xFF1A1A1A),
      shadowColor: isLightTheme ? AppColor.shadowColor.withOpacity(.07) : AppColor.shadowColor.withOpacity(.07),
      smallShadow: [
        if (isLightTheme)
          BoxShadow(
              color: AppColor.shadowColor.withOpacity(.12),
              offset: const Offset(0, 0),
              spreadRadius: 0,
              blurRadius: 7),
        if (!isLightTheme)
          BoxShadow(
              color: AppColor.shadowColor.withOpacity(.30),
              blurRadius: 6,
              offset: const Offset(1, 2))
      ],
      wideShadow: [
        if (isLightTheme)
          BoxShadow(
              color: AppColor.shadowColor.withOpacity(.25),
              offset: const Offset(0, 0),
              spreadRadius: 0,
              blurRadius: 7),
        if (!isLightTheme)
          BoxShadow(
              color: AppColor.shadowColor.withOpacity(.2),
              spreadRadius: 2,
              blurRadius: 15,
              offset: const Offset(3, 5))
      ],
    );
  }
}

// A class to manage specify colors and styles in the app not supported by theme data
class ThemeColor {
  Color backgroundColor;
  Color textColor;
  Color defaultColor;
  Color cardColor;
  Color shadowColor;
  List<BoxShadow> smallShadow;
  List<BoxShadow> wideShadow;

  ThemeColor({
    required this.backgroundColor,
    required this.defaultColor,
    required this.textColor,
    required this.shadowColor,
    required this.cardColor,
    required this.smallShadow,
    required this.wideShadow,
  });
}

