import 'package:flutter/cupertino.dart';
import 'package:github_repo_search/models/repo.dart';
import 'package:github_repo_search/models/responses/get_all_repo_response.dart';

class RepoProvider with ChangeNotifier{

  List<Repo>? repos;
  GetAllRepoResponse? getAllRepoResponse;

  void setRepos(List<Repo>? repos){
    this.repos = repos;

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      notifyListeners();
    });

  }

  void setAllRepoResponse(GetAllRepoResponse? getAllRepoResponse){
    this.getAllRepoResponse = getAllRepoResponse;
  }

  //singleton
  static final _currentUser = RepoProvider._internal();
  factory RepoProvider(){
    return _currentUser;
  }
  RepoProvider._internal();
}