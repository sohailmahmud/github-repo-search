import 'package:github_repo_search/models/repo.dart';

class GetAllRepoResponse {
  int? totalCount;
  bool? incompleteResults;
  List<Repo>? repos;

  GetAllRepoResponse({this.totalCount, this.incompleteResults, this.repos});

  GetAllRepoResponse.fromJson(Map<String, dynamic> json) {
    totalCount = json['total_count'];
    incompleteResults = json['incomplete_results'];
    if (json['items'] != null) {
      repos = <Repo>[];
      json['items'].forEach((v) {
        repos!.add(Repo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['total_count'] = totalCount;
    data['incomplete_results'] = incompleteResults;
    if (repos != null) {
      data['items'] = repos!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}