import 'package:flutter_dotenv/flutter_dotenv.dart';

class Environment {
  static String get baseUrl => dotenv.env['BASE_URL'] ?? '';
  static String get environment => dotenv.env['ENV'] ?? '';
  static String get apiKey => dotenv.env['API_KEY'] ?? '';
}
