import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:animations/animations.dart';
import 'package:github_repo_search/main.dart';
import 'package:github_repo_search/utilities/colors.dart';
import 'package:intl/intl.dart';

enum AppBorderSide {none, left, right, top, bottom,topLeft,topRight,bottomLeft,bottomRight}

class Utility{
  static get context => RootApp.navKey.currentContext!;
  static pop() => Navigator.of(context).pop();

  static Future<bool> isInternetConnected() async{
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
      return false;
    } on SocketException catch (_) {
      return false;
    }
  }

  static String formatDateTime(DateTime dateTime){
    return DateFormat("MM-dd-yyyy hh:mm aa").format(dateTime);
  }

  static showSnackBar(String value,{Function()? onRetry,int durationInSeconds= 2}){
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: Duration(seconds: durationInSeconds),
      margin: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
      padding: onRetry==null? const EdgeInsets.all(12.0) :const EdgeInsets.only(left: 12),
      behavior: SnackBarBehavior.floating,
      content: Text(value,style: const TextStyle(fontSize: 17,color: Colors.white,fontWeight: FontWeight.w500),),
      backgroundColor: AppColor.defaultColor,
      action: onRetry==null? null : SnackBarAction(label: 'Retry',textColor: Colors.white60,onPressed: onRetry,),
    ));
  }


  static showLoadingDialog(){
    showModal(
        configuration: const FadeScaleTransitionConfiguration(barrierDismissible: false),
        context: context,
        builder:(context) => const SpinKitCircle(size: 85,color: AppColor.defaultColor,)
    );
  }

  static String getTimeGreeting(){
    String greeting = "";

    int hour = DateTime.now().hour;

    if(hour>= 12 && hour < 17){
      greeting = "Good Afternoon";
    } else if(hour >= 17 && hour < 24){
      greeting = "Good Evening";
    } else {
      greeting = "Good Morning";
    }

    return greeting;
  }
}

extension StringCasingExtension on String {
  String toCapitalized() => length > 0 ?'${this[0].toUpperCase()}${substring(1).toLowerCase()}':'';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ').split(' ').map((str) => str.toCapitalized()).join(' ');
}