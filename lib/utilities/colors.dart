import 'package:flutter/material.dart';

class AppColor{
  static const Color defaultColor = Color(0xFF2962FF);
  static const Color greyColor = Color(0xFFBEBEBE);


  static Color get baseBlue{
    return const Color(0xFF2962FF);
  }

  static Color get shadowColor{
    return const Color(0xFF464C88);
  }

  static Color get contrast4{
    return const Color(0xFF1D51E1);
  }

  static Color get contrast3{
    return const Color(0xFF1443C5);
  }

  static Color get fgBlue{
    return const Color(0xFFEBEFFA);
  }

  static Color get bgBlue{
    return const Color(0xFFF2F2FA);
  }

  static Color get fullWhite{
    return const Color(0xFFFFFFFF);
  }

  static Color get fullBlack{
    return const Color(0xFF000000);
  }

  static Color get baseBlack{
    return const Color(0xFF383838);
  }

  static Color get black2{
    return const Color(0xFF4A4A4A);
  }

  static Color get blackDisable{
    return const Color(0xFF8D8D8D);
  }

  static Color get black3{
    return const Color(0xFFCDCDCD);
  }

  static Color get black4{
    return const Color(0xFFE8E8E8);
  }

  static Color get black1_1{
    return const Color(0xFF16151A);
  }

  static Color get black1_2{
    return const Color(0xFF1F1E25);
  }

  static Color get fadedRed{
    return const Color(0xFFF3C5C5);
  }

  static Color get red{
    return const Color(0xFFDA1E28);
  }

  static Color get carbonSelect{
    return const Color(0xFF404758);
  }

  static Color get carbon1{
    return const Color(0xFF9A9FAC);
  }

  static Color get carbon2{
    return const Color(0xFFC6C9CF);
  }
}

class AppGradient{
  static LinearGradient getColorGradient(String name){

    switch(name){
      case "default" : {
        return const LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors:  [
              Color(0xFF00AFFF),
              Color(0xFF0051FF),
            ]);
      }
      case "defaultReverse" : {
        return const LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors:  [
              Color(0xFF0051FF),
              Color(0xFF00AFFF),
            ]);
      }
      case "button" : {
        return const LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors:  [
              Color(0xFF075AFB),
              Color(0xFF64FFC7),
            ]);
      }
      default: {
        //default
        return const LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors:  [
              Color(0xFF00AFFF),
              Color(0xFF0051FF),
            ]);
      }
    }

  }
}
