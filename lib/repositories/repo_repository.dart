import 'dart:convert';
import 'package:github_repo_search/models/api_response.dart';
import 'package:github_repo_search/models/repo.dart';
import 'package:github_repo_search/repositories/api_constants.dart';
import 'package:github_repo_search/repositories/local_repositories/local_repo_repository.dart';
import 'package:github_repo_search/utilities/utility.dart';
import 'package:http/http.dart' as http;


class RepoRepository{

  static Future<APIResponse<List<Repo>>> getRepos(String searchText) async{
    if(!await Utility.isInternetConnected()){
      // return APIResponse<List<RestaurantTable>>(error: true, message: "Internet is not connected!");

      APIResponse<List<Repo>> response = await LocalRepoRepository.getRepos();

      return response;

    }

    print(headersWithAuth);

    Uri url = Uri.parse('${baseUrl}search/repositories?q=$searchText&sort=stars&order=desc');
    return http.get(url,headers: headersWithAuth).then((data) async{
      //print(data.body);
      final responseData = utf8.decode(data.bodyBytes);
      final jsonData = json.decode(responseData);
      print(jsonData);
      if(jsonData["message"] != null){
        List<Repo> repos = [];
        for(int i=0; i<jsonData['items'].length; i++){
          repos.add(Repo.fromJson(jsonData['items'][i]));
        }

        await LocalRepoRepository.saveRepos(jsonData['items']);

        return APIResponse<List<Repo>>(data: repos);
      }
      return APIResponse<List<Repo>>(error: true, message:jsonData["message"]);
    }).catchError((onError){
      print(onError);
      return APIResponse<List<Repo>>(error: true, message: "An Error Occurred!");
    });
  }

}