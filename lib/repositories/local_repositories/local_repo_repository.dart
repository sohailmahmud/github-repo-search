import 'dart:convert';
import 'package:github_repo_search/models/api_response.dart';
import 'package:github_repo_search/models/repo.dart';
import 'package:hive/hive.dart';


class LocalRepoRepository{

  static Future<APIResponse<bool>> saveRepos(String data) async {
    try{
      final userHive = await Hive.openBox('localData');
      userHive.put('repos', data);
      return APIResponse<bool>(data: true);
    }catch(e){
      print(e);
      return APIResponse<bool>(error: true, message: "An Error Occurred!");
    }
  }

  static Future<APIResponse<List<Repo>>> getRepos() async {
    try{
      final userHive = await Hive.openBox('localData');
      final showData = userHive.get('repos');
      print(showData);


      if(showData != null){
        final jsonData = json.decode(showData);

        List<Repo> repos = [];
        for(int i=0; i<jsonData['data'].length; i++){
          repos.add(Repo.fromJson(jsonData['data'][i]));
        }

        return APIResponse<List<Repo>>(data: repos);

      }else{
        return APIResponse<List<Repo>>(error: true, message: "No data is saved!");
      }


    }catch(e){
      print(e);
      return APIResponse<List<Repo>>(error: true, message: "An Error Occurred!");
    }
  }

}