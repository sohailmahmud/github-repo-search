
import 'package:github_repo_search/utilities/environment.dart';

String get baseUrl{
  return Environment.baseUrl;
}

final headers = {
  "Content-Type": "application/json",
  "Accept": "application/json",
};

final headersWithAuth = {
  'Content-Type': 'application/json',
  'Authorization': 'Bearer ${Environment.apiKey}',
};

