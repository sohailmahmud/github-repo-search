# github-repo-search



## App Screenshots

| | | |
|:-------------------------:|:-------------------------:| :-------------------------:|
|<img width="1604" alt="app_screenshot_01.png" src="https://gitlab.com/sohailmahmud/github-repo-search/-/raw/main/media/app_screenshot_01.png" >  App Screenshot 01| <img width="1604" alt="app_screenshot_02.png" src="https://gitlab.com/sohailmahmud/github-repo-search/-/raw/main/media/app_screenshot_02.png" > App Screenshot 02| <img width="1604" alt="app_screenshot_03.png" src="https://gitlab.com/sohailmahmud/github-repo-search/-/raw/main/media/app_screenshot_03.png" > App Screenshot 03|
|<img width="1604" alt="app_screenshot_04.png" src="https://gitlab.com/sohailmahmud/github-repo-search/-/raw/main/media/app_screenshot_04.png" >  App Screenshot 04| <img width="1604" alt="app_screenshot_05.png" src="https://gitlab.com/sohailmahmud/github-repo-search/-/raw/main/media/app_screenshot_05.png" > App Screenshot 05|


## Directory layout

### Base skeleton

```
`-- lib
    `-- <directory> 
        |-- models 
        |-- providers
        |-- repositories
        |-- utilities
        |-- views
        `-- main.dart
        
```

### Directory structure

| Directory | Description                                   | 
| :--- |:----------------------------------------------|
| `models` | Contains the data models of the application.  |
| `providers` | Contains the providers of the application.    |
| `repositories` | Contains the repositories of the application. |
| `utilities` | Contains the utilities of the application.    |
| `views` | Contains the views screen of the application. |
| `main.dart` | Contains the entry point of the application.  |


## Used Packages

| Package                                                     | Description                            |
|:------------------------------------------------------------|:---------------------------------------|
| [provider](https://pub.dev/packages/provider)               | State management.                      |
| [path_provider](https://pub.dev/packages/path_provider)     | File system paths.                     |
| [http](https://pub.dev/packages/http)                       | HTTP requests.                         |
| [hive](https://pub.dev/packages/hive)                       | Key-value database.                    |
| [flutter_spinkit](https://pub.dev/packages/flutter_spinkit) | Loading spinner.                       |
| [flutter_dotenv](https://pub.dev/packages/flutter_dotenv)   | Environment variables.                 |
| [intl](https://pub.dev/packages/intl)                       | Internationalization and localization. |
| [test](https://pub.dev/packages/test)                       | Testing library.                       |
| [build_runner](https://pub.dev/packages/build_runner)       | Code generation.                       |
| [animations](https://pub.dev/packages/animations)           | Animations.                            |
| [shimmer](https://pub.dev/packages/shimmer)                 | Shimmer effect.                        |
| [infinite_scroll_pagination](https://pub.dev/packages/infinite_scroll_pagination) | Infinite scroll. |


## Unfinished Tasks

- [ ] Custom pagination.
- [ ] Custom error handling.
- [ ] Data refreshing.

## Future Scope

- [ ] Custom Unit Testing.
- [ ] Custom Integration Testing.
- [ ] Custom Error Handling.
- [ ] Custom Pagination.
- [ ] Data Refreshing.
- [ ] Organizations search.
- [ ] Users search.
- [ ] Trending repository search.
- [ ] Trending developers search.
